package rinatBorz;


import robocode.*;
import robocode.ScannedRobotEvent;
import robocode.AdvancedRobot;

import java.awt.Color;
import java.util.Random;


public class Borz extends AdvancedRobot {
    private byte radarDirection = 1;
    private byte moveDirection = 1;

    private final ExtendsFromFiendeBot enemy = new ExtendsFromFiendeBot();
    private final NormaliseraRiktning advantage = new NormaliseraRiktning();
    private final AbsolutMoving moveCalculation = new AbsolutMoving();
    Random random = new Random();

    public void run() {

        setColors(Color.gray, Color.black, Color.black);
        setAdjustRadarForRobotTurn(true);
        setAdjustRadarForGunTurn(true);

        while (true) {
            if (getOthers() > 1 && getOthers() <= 4) {
                doMove2();
                doRadar();
                doGun();
                execute();
                System.out.println("rör sig med större marginal och kör saktare framåt!!");
            }
            else if (getOthers() > 4){
                doMove3();
                doRadar();
                doGun();
                execute();
            } else {
                doMove();
                doRadar();
                doGun();
                execute();
                System.out.println("en mot en, gör häftiga rörelse och jagar efter motståndare!!");
            }


        }
    }


    public void onScannedRobot(ScannedRobotEvent e) {

        if (enemy.none() || e.getDistance() < enemy.getDistance() - 70 ||
                e.getName().equals(enemy.getName())) {
            enemy.update(e, this);
        }

    }


    void doRadar() {
        if (enemy.none()) {
            setTurnRadarRight(360);
        } else {
            double turn = getHeading() - getRadarHeading() + enemy.getBearing();
            turn += 20 * radarDirection;
            setTurnRadarRight(advantage.normalizeBearing(turn));
            radarDirection *= -1;
        }
    }

    public void doMove() {
        setTurnRight(advantage.normalizeBearing(enemy.getBearing() + 90 - (130 * moveDirection)));

        if (enemy.getDistance() < 169) {
            setTurnRight(advantage.normalizeBearing(enemy.getBearing() + 90 - (-100 * moveDirection)));
        }

        if (getTime() % 20 == 0) {
            moveDirection *= -1;
            setAhead(300 * moveDirection);
        }


    }

    public void doMove2() {
        setTurnRight(advantage.normalizeBearing(enemy.getBearing() + 90 - (46 * moveDirection)));


        if (enemy.getDistance() < 210) {
            setTurnRight(advantage.normalizeBearing(enemy.getBearing() + 90 - (-100 * moveDirection)));
        }

        if (getTime() % 24 == 0) {
            moveDirection *= -1;
            setAhead(450 * moveDirection);
        }

    }

    public void doMove3() {
        setTurnRight(advantage.normalizeBearing(enemy.getBearing() + 90 - (50 * moveDirection)));

        if (enemy.getDistance() < 220) {

            setTurnRight(advantage.normalizeBearing(enemy.getBearing() + 90 - (-100 * moveDirection)));
        }

        if (getTime() % 20 == 0) {
            moveDirection *= -1;
            setAhead(480 * moveDirection);


        }

    }

    void doGun() {
        if (enemy.none())
            return;
        double firePower = Math.min(400 / enemy.getDistance(), 3);
        double bulletSpeed = 20 - firePower * 3;
        long time = (long) (enemy.getDistance() / bulletSpeed);


        double futureX = enemy.getFutureX(time);
        double futureY = enemy.getFutureY(time);

        double absDeg = moveCalculation.absoluteBearing(getX(), getY(), futureX, futureY);

        if (getGunHeat() == 0 && Math.abs(getGunTurnRemaining()) < 10)
            setFire(Math.min(500 / enemy.getDistance(), 3));
        setFire(Math.min(800 / enemy.getDistance(), 2));
        setTurnGunRight(advantage.normalizeBearing(absDeg - getGunHeading()));

    }


    public void onHitWall(HitWallEvent e) {
        if (getVelocity() == 0)
        setAhead(-300 * moveDirection);

    }



    public void onRobotDeath(RobotDeathEvent e) {
        if (e.getName().equals(enemy.getName())) {
            enemy.reset();
        }
    }

}